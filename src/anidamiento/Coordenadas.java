package anidamiento;

	import java.util.ArrayList;

	public class Coordenadas {  //Clase externa
		//Atributos de la clase externa:
	    private ArrayList<Punto> listaPuntos;

	    class Punto {	//Clase interna
	    	//Atributos de la clase interna:
	        private int x, y;

	        //Metodos de la clase interna:
	        public Punto(int x, int y) {
	            this.setX(x);
	            this.setY(y);
	            
	        }

	        public int retornarCuadrante() {
	            if (x > 0 && y > 0)
	                return 1;
	            else if (x < 0 && y > 0)
	                return 2;
	            else if (x < 0 && y < 0)
	                return 3;
	            else if (x > 0 && y < 0)
	                return 4;
	            else
	                return -1;
	        }

			public int getX() {
				return x;
			}
			public void setX(int x) {
				this.x = x;
			}
			public int getY() {
				return y;
			}
			public void setY(int y) {
				this.y = y;
			}

			@Override
			public String toString() {
				return "Punto [x=" + x + ", y=" + y + "]";
			}
	    }
	    
	    
	  //Metodos de la clase externa:
	    public Coordenadas() {
	    	listaPuntos = new ArrayList<Punto>();
	    }

	    public void agregarPunto(int x, int y) {
	    	listaPuntos.add(new Punto(x, y));
	    }

	    public int cantidadPuntosCuadrante(int cuadrante) {
	        int cant = 0;
	        for (Punto pun : listaPuntos)
	            if (pun.retornarCuadrante() == cuadrante)
	                cant++;
	        return cant;
	    }

		public ArrayList<Punto> getListaPuntos() {
			return listaPuntos;
		}

		public void setListaPuntos(ArrayList<Punto> listaPuntos) {
			this.listaPuntos = listaPuntos;
		}

		@Override
		public String toString() {
			return "Coordenadas [puntos=" + listaPuntos + "]";
		}
	}

	    	   
	 
	    
	

