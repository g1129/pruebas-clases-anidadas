package anidamiento;

import anidamiento.Coordenadas.Punto;

public class Inicio{

public static void main(String[] args) {
	anidamiento();
}

public static void anidamiento() {
    Coordenadas coordenadas = new Coordenadas();
    coordenadas.agregarPunto(30, 30);
    coordenadas.agregarPunto(2, 7);
    coordenadas.agregarPunto(-3, 2);
    coordenadas.agregarPunto(-5, -4);
    coordenadas.agregarPunto(-9, -2);
    System.out.println("Cantidad de puntos en el primer cuadrante:"
            + coordenadas.cantidadPuntosCuadrante(1));
    System.out.println("Cantidad de puntos en el segundo cuadrante:"
            + coordenadas.cantidadPuntosCuadrante(2));
    System.out.println("Cantidad de puntos en el tercer cuadrante:"
            + coordenadas.cantidadPuntosCuadrante(3));
    System.out.println("Cantidad de puntos en el cuarto cuadrante:"
            + coordenadas.cantidadPuntosCuadrante(4));
    //

    // Creaci�n directa de objetos de la clase interna, desde fuera de 
    // la clase externa (requiere crear al menos una instancia de la externa:
    Coordenadas.Punto p1 = coordenadas.new Punto(100,100);
    Coordenadas.Punto p2 = coordenadas.new Punto(200,200);
    System.out.println("Punto p1= " + p1);
    System.out.println("Punto p2= " + p2);
    System.out.println("Contenido de 'listaPuntos' del objeto 'coordenadas': ");
    for (Punto punto : coordenadas.getListaPuntos()) {
    	System.out.println("   --> " + punto);
    }
}
}